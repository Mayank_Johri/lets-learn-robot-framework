# Executing Test Cases

## Basic usage

Normally the test cases are executed from command line using `robot` command and the execution report are stored in `XML` & `HTML` formats.

## Test execution

Tests can be executed using followings ways

### Python

Once robot framework is installed, `robot` files can be executed using one of the following methods.

#### `robot` command

`robot [options] data_sources`

**As `options` are same for everytype we will cover them later in this chapter**.

**Example:**

```bash
$:> robot 1_basic.robot
```

**Output:**

```
==============================================================================
1 Basic                                                                       
==============================================================================
Validate count of array                                               | PASS |
------------------------------------------------------------------------------
1 Basic                                                               | PASS |
1 critical test, 1 passed, 0 failed
1 test total, 1 passed, 0 failed
==============================================================================
Output:  /books/mj/books/llrf/code/lists/output.xml
Log:     /books/mj/books/llrf/code/lists/log.html
Report:  /books/mj/books/llrf/code/lists/report.html
```

#### robot as python module

We can also run the robot scripts aka data_sources using robot as module as shown below

```bash
$:> python -m robot [options] data_sources
```

 ### Using Java 

```bash
$:> java -jar robotframework.jar [options] data_sources
```

### Selecting Testcases for execution

`robot` provides multiple ways to select the testcases/tasks to execute. We are going to cover few of them in this section.

#### Explicit Testcases Execution

We can explicitly provide testcases to be executed as shown in the example below. 

```bash
$:> robot 1_test.robot
```

We can also specify more than one testcase files as shown below

```bash
$:> robot 1_basic.robot 2_basics.robot 
```

We can also specify directories 

```
$:> robot smoke_tests/ ui_tests/ selenium_tests/ backend_tests/
```

When we are running the testcases at folder level, than folder becomes the `test suite` and all the testcases under it becomes testcases under it and are arranged as per the folder structure.

``` bash
basic
|____err
| |____1_err_basic.robot
|____1_basic.robot
```

We have `basic` as folder under it we have a file named `1_basic.robot` and a folder named `err`, inside `err` subfolder we have a file named `1_err_basic.robot`. So when robot runs the testcases, it treats `1` as Suite with `1_basic.robot` and `err` as child test suites and `err` also contains another child suite named `1_err_basic.robot`.

We can also use wild card for selecting testcases

```bash
$:> robot 1_* 4_*
```

or, using regex such as 

```bash
$:> robot *diction*
```

**Output:**

```bash
==============================================================================
5.2.1 Dictionary For & 5.2 Dictionary For                                     
==============================================================================
5.2.1 Dictionary For & 5.2 Dictionary For.5.2.1 Dictionary For                
==============================================================================
Simple For Loop                                                       {u'APPLE': u'apple', u'MANGO': u'mango', u'GRAPES': u'grapes'}
Simple For Loop                                                       | PASS |
------------------------------------------------------------------------------
Advance For Loop                                                      | FAIL |
No keyword with name 'Get From Dictionary' found.
------------------------------------------------------------------------------
5.2.1 Dictionary For & 5.2 Dictionary For.5.2.1 Dictionary For        | FAIL |
2 critical tests, 1 passed, 1 failed
2 tests total, 1 passed, 1 failed
==============================================================================
5.2.1 Dictionary For & 5.2 Dictionary For.5.2 Dictionary For                  
==============================================================================
Simple For Loop                                                       APPLE
MANGO
GRAPES
Simple For Loop                                                       | PASS |
------------------------------------------------------------------------------
List for loop                                                         APPLE
MANGO
GRAPES
Watermelon
Orange
Pear
Cherry
List for loop                                                         | PASS |
------------------------------------------------------------------------------
5.2.1 Dictionary For & 5.2 Dictionary For.5.2 Dictionary For          | PASS |
2 critical tests, 2 passed, 0 failed
2 tests total, 2 passed, 0 failed
==============================================================================
5.2.1 Dictionary For & 5.2 Dictionary For                             | FAIL |
4 critical tests, 3 passed, 1 failed
4 tests total, 3 passed, 1 failed
==============================================================================
Output:  /books/mj/books/llrf/code/lists/output.xml
Log:     /books/mj/books/llrf/code/lists/log.html
Report:  /books/mj/books/llrf/code/lists/report.html
```

and 

```bash
$:> robot *[st].robot
```

### Options

Robot Framework provides lots of options which can be used to control how the testcases should execute and how the reporting should happen. We are going to cover most common options.

**One this to note is that options should be before data source (test suite/testcases) files.**

- Robot Framework support both `short` & `long` format of arguments as options
- Options accepting no values can be **disabled** by using the same option again with `no` prefix added or dropped. 
- In case, same option is called multiple times, than the last instance of option has precedence.
- Many options can take patterns as shown below
  - `*` matches any string, including an empty string.
  - `?` matches any **single** character.
  - `[abc]` matches **one character** in the bracket.
  - `[!abc]` matches **one character not** in the bracket.
  - `[a-z]` matches one character from the range in the bracket.
  - `[!a-z]` matches one character not from the range in the bracket.
  - Unlike with glob patterns normally, path separator characters `/` and \ and the newline character `\n` are matches by the above wildcards.
  - Unless noted otherwise, pattern matching is **case, space, and underscore insensitive**.
- 

#### Tag patterns

 Most of tag related options can also accept arguments as *tag patterns* and can also accept `AND`, `OR` & `NOT` Operators as shown in below examples.

##### AND

When we need that both the values should be present for any given argument, then we use it 

```bash
--include xpANDsp2        # both XP and SP2 should be present
```

##### OR

When we need only even one of the values to be present for any given argument, than we use it. 

```bash
--include xpORwin7        # either WinXP or Win7
```

##### NOT

When don't need the values to be preset for any given argument, namely the testcases, than we use it. 

```bash
--include NOTXP      # should not contain XP
```

### `ROBOT_OPTIONS` and `REBOT_OPTIONS` environment variables

We can also use `ROBOT_OPTIONS` and `REBOT_OPTIONS` environment variables to specify global default options for our testcases & result post processing respectivly.

### Test Results

#### Command Line output

When we run the robot testcases, than we can view the real-time status on the console as shown below.

```
robot 1.1_free_named_arguments.robot 
==============================================================================
1.1 Free Named Arguments                                                      
==============================================================================
Valid ints can be added                                               
30 - {u'c': u'102', u'd': u'12'}
Valid ints can be added                                               | PASS |
------------------------------------------------------------------------------
1.1 Free Named Arguments                                              | PASS |
1 critical test, 1 passed, 0 failed
1 test total, 1 passed, 0 failed
==============================================================================
```

#### Generated output files

As a result to execution, three files are generated by `robot framework`. 

- `XML` file: Captures all the information about test execution
- `Log` file: Details execution log file
- `HTML Report` file: Details HTML report for users.

### Argument Files

We can also use argument file to pass the arguments to execute the robot testcases. It can be passed to robot using `--argumentfile <file_name>` or `-A <file_name>`

#### Syntax

Argument file can contain both `arguemnts` & `path of testcases` and one argument is passed per-line as shown below

```bash
--variable sample:True
# Below is the path
/path/for/testcases
```

The Operator & its corresponding value should be seperated by one of the following ways. 

- Space:    (—variable sample)
- `=` Sign  (—variable==sample)
- Multiple Spaces  (—variable    sample)

### Randomize Execution Order

The test execution order can be randomized using option `--randomize <what>[:<seed>]`, where `<what>` is one of the following:

- `tests`

  Test cases inside each test suite are executed in random order.

- `suites`

  All test suites are executed in a random order, but test cases inside suites are run in the order they are defined.

- `all`

  Both test cases and test suites are executed in a random order.

- `none`

  Neither execution order of test nor suites is randomized. This value can be used to override the earlier value set with --randomize.



