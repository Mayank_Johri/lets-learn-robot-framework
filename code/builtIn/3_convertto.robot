

*** Test Cases ***
Validating Conversions

    ${val1} =   Convert To Hex   999
    Log To Console   \nConverting to Hex 999 : ${val1}
    ${val1} =   Convert To Binary   999
    Log To Console   Converting to binary ${val1}
    ${val1} =   Convert To Octal    999
    Log To Console   Converting to Octal 999 : ${val1}
    ${val1} =   Convert To Integer   0x3E7
    Log To Console   Converting to Integer 3E7 : ${val1}
    ${val1} =   Convert To Integer   0b101010
    Log To Console   Converting to Integer 0b101010 : ${val1}
    ${val1} =   Convert To Integer   0o36
    Log To Console   Converting to Integer 0o36 : ${val1}
