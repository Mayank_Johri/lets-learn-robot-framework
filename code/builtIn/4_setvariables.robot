*** KeyWords ***
Get Area
    ${ret} =    Evaluate    ${width} * ${height}
    [return]   ${ret}

*** Test Cases ***
Validate Set Test Vari
    Set Test Variable   ${width}   100
    Set Test Variable   ${height}   40
    ${ret} =    Get Area
    Log To Console   \n${ret}

 

