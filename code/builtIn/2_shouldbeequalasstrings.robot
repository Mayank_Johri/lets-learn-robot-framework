*** Variables ***
${v} =    100


*** KeyWords ***
Get Area of Rect with ${height} and ${width} size
    ${ret}    Set Variable   ${height} * ${width}
    [return]    ${ret}

*** Test Cases ***
Validate Should be Equal
    ${res} =    Get Area of Rect with 10 and 200 size
    Log To Console   ${res}
    Should Be Equal As Strings    ${res}   10 * 200
    
