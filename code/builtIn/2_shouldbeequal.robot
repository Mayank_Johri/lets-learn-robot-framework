*** Variables ***
${v} =    100


*** KeyWords ***
Get Area of Rect with ${height} and ${width} size
    ${ret}    Evaluate   ${height} * ${width}
    [return]    ${ret}

*** Test Cases ***
Validate Should be Equal
    ${res} =    Get Area of Rect with 10 and 200 size
    Log To Console   ${res}
    ${expected} =    Convert To Integer   2000
    Should Be Equal   ${res}   ${expected}
    
