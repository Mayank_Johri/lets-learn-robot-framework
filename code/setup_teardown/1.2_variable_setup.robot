# -*- coding: robot -*-

*** Settings ***
Documentation    By using the command line option we can chanage the `Setup` 
...                keywords value.

*** Keywords ***
RunOnce
    Log To Console    This is run once

RunSecond
    Log To Console    This is another.

*** Variables ***
${vari}    RunOnce

 
*** Test Cases ***
A Sample Test Case
    [Setup]   ${vari}
    No Operation


