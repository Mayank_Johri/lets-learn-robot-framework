# -*- coding: robot -*-

*** Settings ***
Library    OperatingSystem
Test Setup        Create File   ${file_name}
Test TearDown     Run Keywords    Remove File   ${file_name}
                  ...   AND   Log To Console   Deleted the file: ${file_name}

*** Variables ***
${file_name}=  sum_total.txt

*** Keywords ***
Add Int
    [Arguments]    ${a}    ${b}
    ${ret} =    Evaluate   ${a} + ${b}
    [return]   ${ret}

Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       add int   ${val1}   ${val2}
    [return]       ${ret}

My Custom Setup
    Log to Console   "This is custom Setup"

My Custom Teardown
    Fail
    Log to Console   "This is custom Teardown"

*** Test Cases ***
Valid ints can be added
    [Documentation]   Both Setting's Setup and Teardown will run on it.
    ${ret} = 	Please Add Int    10    100
    Should Be Equal As Strings   ${ret}  110


Valid Sum of ints with custom Setup 
    [Documentation]   Custom Setup and Setting's Teardown will run on it.
    ${ret} = 	Please Add Int    10    100
    Should Be Equal As Strings   ${ret}  110
    [Setup]   Run Keyword   My Custom Setup

Valid Sum of ints with custom teardown 
    [Documentation]   Custom Teardown and Setting's Setup will run on it.
    ${ret} = 	Please Add Int    10    100
    Should Be Equal As Strings   ${ret}  110
    [Teardown]   Run Keyword   My Custom Teardown

Valid ints can be added without teardown
    ${ret} = 	Please Add Int    101   102
    Should Be Equal As Strings   ${ret}  203
    [Teardown]   NONE     # It will disable Teardown


Valid ints can be added without setup
    [Setup]     # It will disable setup  
    ${ret} = 	Please Add Int    101   102
    Should Be Equal As Strings   ${ret}  203
