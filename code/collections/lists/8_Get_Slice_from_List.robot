*** Settings ***
Library   Collections

*** Variables ***
@{fruits}    apple   grapes   pineapple   Jackfruit   Mango

*** Test Cases ***
Validate Append the list
    ${fruit} =    Get Slice From List   ${fruits}   1   3
    Log To Console   \n${fruit}

Validate Slice the list
    [Documentation]    This testcase will fail as step is not available 
    ...   Robot Framework
    ${fruit} =    Get Slice From List   ${fruits}   0    5   2
    Log To Console   \n${fruit}
