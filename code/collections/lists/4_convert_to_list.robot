*** Settings ***
Library   Collections

*** Variables ***
${fish}   shark   dolphine

*** Test Cases ***
Validate Combine List
    ${lst} =    Convert To List   ${fish}
    Log To Console   ${lst} 

Another Example
    ${fish}     Set Variable    shark    dolphine
    @{lst} =    Convert To List   ${fish}
    Log To Console   ${lst} 

