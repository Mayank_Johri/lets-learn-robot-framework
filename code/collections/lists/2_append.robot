*** Settings ***
Library   Collections

*** Variables ***
@{fruits}    apple   grapes

*** Test Cases ***
Validate Append the list
    Log To Console   \n${fruits}
    Append To List   ${fruits}   mango
    Append To List   ${fruits}   pineapple   Jackfruit
    Log To Console   \n${fruits}

