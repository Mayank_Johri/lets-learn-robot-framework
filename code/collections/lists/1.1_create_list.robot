*** Test Cases ***
Validate Create List
    @{lst}   Create List
    Log To Console   ${lst}

Validate Create List with data
    @{lst}   Create List    1   2  3
    Log To Console   ${lst}
