*** Settings ***
Library   Collections

*** Variables ***
@{fruits}    apple   grapes   pineapple   Jackfruit   Mango
@{fri}    apple   Mango

*** Test Cases ***
Validate Remove Values from list
    Remove Values from List   ${fruits}   pineapple
    Log To Console    ${fruits}

Validate Remove from list
    Remove from List   ${fruits}   2
    Log To Console    ${fruits}

