*** Settings ***
Library   Collections

*** Variables ***
@{fruits}    apple   grapes   pineapple   Jackfruit   Mango
@{fri}    apple   Mango

*** Test Cases ***
Validate Set Values in list
    Set List Value    ${fruits}   4    Orange
    Log To Console    ${fruits}

Validate Reverse list
    Reverse List      ${fruits}
    Log To Console    ${fruits}

Validate Sort list
    Sort List     ${fruits}
    Log To Console    ${fruits}

