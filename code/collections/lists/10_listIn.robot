*** Settings ***
Library   Collections

*** Variables ***
@{fruits}    apple   grapes   pineapple   Jackfruit   Mango
@{fri}    apple   Mango

*** Test Cases ***
Validate if sublist in list
    List Should Contain Sub List   ${fruits}   ${fri}

Validate if list contain value
    List Should Contain Value   ${fruits}   apple


Validate if list should not contain value
    List Should Not Contain Value   ${fruits}   orange


Validate Copy List
    @{cp}    Copy List   ${fruits}
    Lists Should Be Equal    ${cp}    ${fruits}


