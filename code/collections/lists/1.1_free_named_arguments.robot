# -*- coding: robot -*-

*** Keywords ***
Multiply
    [Documentation]   This is *"Multiply"* keyword for testcase _"${TEST_NAME}'s"_
    ...     Argument Provided:
    ...         ${a} -  First Int
    ...         ${b} -  Second Int
    ...         &{p} -  Misc Values
    [Arguments]    ${a}   ${b}    &{p}
    ${ret} =       Evaluate   ${a}*${b}
    Log To Console     \n${ret} - &{p}
    [return]       ${ret}

*** Test Cases ***
Validate If Two Ints Can Be Multiplied
    [Documentation]    It tests multiplication of two ints
    ...    It also provides multiple arguments which are not used.
    ${ret} =    Multiply   a=10   b=3   c=102   d=12
    Should Be Equal As Strings      ${ret}   30

