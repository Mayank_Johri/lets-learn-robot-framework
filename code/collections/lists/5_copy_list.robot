*** Settings ***
Library   Collections

*** Variables ***
@{fruits}    apple   grapes

*** Test Cases ***
Display the list
    @{cp}    Copy List   ${fruits}
    Append To List   ${cp}    JackFruit
    Log To Console   ${fruits}
    Log To Console   ${cp}
