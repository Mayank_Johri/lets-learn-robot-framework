*** Settings ***
Library   Collections


*** Test Cases ***
Dictionary Basics 
    ${color}    Create Dictionary
    Set To Dictionary    ${color}    Yellow	\#FFFF00
    Set To Dictionary    ${color}    Olive	\#808000
    Set To Dictionary    ${color}    Green	\#008000
    Log To Console   ${color}
