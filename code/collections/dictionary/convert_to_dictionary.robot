*** Settings ***
Library           Collections

*** Variables ***

*** Test Cases ***
ConvertToList
    ${list} =    Create List    aaaa    bbbb    cccc    dddd
    ${dict} =    Create Dictionary
    ...                      list    ${list}
    Log to console    ${dict}
    Should Be True    type($dict) is not dict
    
    ${dict} =    Convert To Dictionary   ${dict}
    Log to console    ${dict}
    Should Be True    type($dict) is dict

