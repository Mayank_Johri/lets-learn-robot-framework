*** Settings ***
Library   Collections

*** Variables ***
# There should be no spaces between `key=value`
&{color}    Yellow=\#FFFF00   Olive=\#808000    Green=\#008000

*** Test Cases ***
Dictionary Basics 
    ${color}    Create Dictionary
    Set To Dictionary    ${color}    Yellow	\#FFFF00
    Set To Dictionary    ${color}    Olive	\#808000
    Set To Dictionary    ${color}    Green	\#008000
    Log To Console   ${color}

Validate Dictionary
    Log To Console   ${color}
    Dictionary Should Not Contain key    ${color}    Blue
    Dictionary Should Not Contain value    ${color}    \#3243443
