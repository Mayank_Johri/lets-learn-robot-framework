*** Settings ***
Library   Collections

*** Variables ***
# There should be no spaces between `key=value`
&{color}    Yellow=\#FFFF00   Olive=\#808000    Green=\#008000

*** Test Cases ***
Dictionary Basics 
    Set To Dictionary    ${color}    Yellow	\#FFFF00
    Set To Dictionary    ${color}    Orange	\#d35400 
    Log To Console   ${color}
    ${cols}   Get Dictionary Values   ${color}
    Log To Console   ${cols}

