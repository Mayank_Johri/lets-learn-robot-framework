*** Settings ***
Library   Dialogs


*** Test Cases ***
Check Password
    ${password} = 	Get Value From User 	Input password   hidden=yes
    Should be Equal as Strings   ${password}   123   *HTML* Sorry, Password <b>${password}</b> is wrong.   values=False
