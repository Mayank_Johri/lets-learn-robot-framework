# -*- coding: robot -*-

** Variables **
@{states}=   UP   MP   KT
@{cap}=    Lucknow   Bhopal    Bangaluru


*** Test Cases ***
Simple For Loop
    FOR    ${state}   ${capital}   IN ZIP   ${states}   ${cap}
	Log To console   ${state}. ${capital}
    END

