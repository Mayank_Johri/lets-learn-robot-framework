*** Settings ***
Library   Collections

** Variables **
&{fruits}=   APPLE=apple   MANGO=mango   GRAPES=grapes


*** Test Cases ***
Simple For Loop
    FOR    ${fruit}   IN   &{fruits}
        Log To console   ${fruit}
    END


** Test Case **
#   Need to find the route cause of the issue
Advance For Loop
    log to console   Advance For Loop
    FOR    ${key}  IN   @{fruits}
        ${val} =   Get From Dictionary   ${fruits}   ${key}
        Log To console   ${key}   ${val}
    END
