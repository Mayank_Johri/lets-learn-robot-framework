# -*- coding: robot -*-

** Variables **
&{fruits}=   APPLE=apple   MANGO=mango   GRAPES=grapes


*** Test Cases ***
Simple For Loop
    FOR    ${fruit}   IN   @{fruits}
	Log To console   ${fruit}
    END

List for loop
    FOR    ${fruit}   IN   @{fruits}    Watermelon   Orange   Pear    Cherry
	   Log To console   ${fruit}
    END

