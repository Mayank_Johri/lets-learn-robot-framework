# -*- coding: robot -*-

** Variables **
@{fruits}=   APPLE   MANGO   GRAPES


*** Keywords ***
Inner loop
    FOR    ${val}    IN RANGE    1  5
        Log to console   ${val}
    END

*** Test Cases ***
Simple For Loop
    FOR     ${fruit}     IN     @{fruits}
	Log To console   ${fruit}
        Inner loop
    END


Error Inner for loop
    FOR    ${fruit}   IN   @{fruits}
	Log To console   ${fruit}
    	FOR   ${val}    IN RANGE    1  5
            Log to console   ${val}
    	END
    END




