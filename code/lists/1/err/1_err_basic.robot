# -*- coding: robot -*-

*** Settings ***
Library  OperatingSystem       # This should be ignored
Library  ../libs/StrArray.py


** Variables **
@{welcome}=  Welcome  to  India.


*** Keywords ***
Get count of list
    [Arguments]  @{welcome}
    ${count} =  echo args lst  @{welcome}
    [Return]  ${count}

*** Test Cases ***
Validate count of array
    ${ret} =    Get count of list      @welcome
    Should Be Equal As Strings      ${ret}  3

