# -*- coding: robot -*-

** Variables **
@{fruits}=   APPLE   MANGO   GRAPES


*** Test Cases ***
Simple For Loop
    FOR    ${index}   ${fruit}   IN ENUMERATE   @{fruits}
	Log To console   ${index}. ${fruit}
    END

