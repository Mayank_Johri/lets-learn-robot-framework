"""."""


class StrArray():

    def echo_args_lst(self, *args):
        for a in args:
            print(a)
        return len(args)

    def echo_args(self, **kwargs):
        """."""
        for key, val in kwargs.items():
            print("key: {key}  - Value: {val}".format(key=key,
                                                      val=val))
        return len(dict(kwargs))

if __name__ == "__main__":
    lst = [1, 2, 3, 4]
    ea = StrArray()
    ea.echo_args_lst(*lst)
    print(ea.echo_args(a=1, b=2, c=3, d=4))
