# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored

*** Test Cases ***
Named-only Arguments
    Run Program    argument    shell=True    # 'shell' is True
 
*** Keywords ***
Run Program
    [Arguments]    @{args}    ${shell}=False
    Log to console    @{args}    shell=${shell}
