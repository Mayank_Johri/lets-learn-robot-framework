# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           Libs/StrArray.py

# ** Variables **
# &{welcome}=    a=Welcome   b=to  c=Bhopal  

*** Keywords ***
Get number of list
    [Arguments]   &{msg}
    ${count} =   echo args   &{msg}    #  a=Welcome   b=to  c=Bhopal
    [Return]  ${count}

*** Test Cases ***
Validate count of array
    ${ret} =    Get number of list   a=Welcome   b=to   c=Bhopal
    Should Be Equal As Strings      ${ret}  3

