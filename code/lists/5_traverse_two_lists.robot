# -*- coding: robot -*-

** Variables **
@{states}=   UP   MP   KT
@{cap}=    Lucknow   Bhopal    Bangaluru


*** Test Cases ***
Simple For Loop
    ${length}=   Get Length   ${states}
    FOR    ${index}  IN RANGE   ${length}
	Log To console   ${states}[${index}]. ${cap}[${index}]
    END

