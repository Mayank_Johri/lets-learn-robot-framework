# -*- coding: robot -*-

** Variables **
@{fruits}=   APPLE   MANGO   GRAPES


*** Test Cases ***
Simple For Loop
    FOR    ${fruit}   IN   @{fruits}
	Log To console   ${fruit}
    END

List for loop
    FOR    ${fruit}   IN   @{fruits}    Watermelon   Orange   Pear    Cherry
	Log To console   ${fruit}
    END

