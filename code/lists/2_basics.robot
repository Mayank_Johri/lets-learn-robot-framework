# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/StrArray.py

** Variables **
@{welcome}=  Welcome  to  India.

*** Keywords ***
Get count of list
    [Arguments]  @{lst}
    ${count} =   echo args lst   @{lst}
    [Return]  ${count}

*** Test Cases ***
Validate count of array
    ${ret} =    Get count of list  Welcome  to  Bhopal      
    Should Be Equal As Strings      ${ret}   3

