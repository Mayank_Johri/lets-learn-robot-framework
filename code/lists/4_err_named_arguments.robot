# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored

*** Test Cases ***
Named-only Arguments
    Run Program    argument    True
 
*** Keywords ***
Run Program
    [Arguments]    @{args}    ${shell}=False
    Log to console    @{args} Shell is  shell=${shell}
