*** Settings ***
Default Tags    smoke test
Force Tags      v_1.0.2    ints

*** Variables ***
${dynamic_tag}       Dynamic Tag

*** Keywords ***
Multiple Ints
    [Arguments]         ${a}    ${b}    ${this_will_not_work_tag}
    [Tags]      validate int    Multiply    ${dynamic_tag}    ${this_will_not_work_tag}
    ${c} =      Evaluate   ${a}*${b}
    [Return]   ${c}


*** Test Cases ***
Multiple Two Ints
    [Tags]      Ints   mulTiply
    ${res} =    Multiple Ints   10   20   This is good
    Log To Console    ${res}
    Should be Equal as Numbers    ${res}   200


Adding Two Ints
    [Tags]
    ${res} =    Evaluate   10 + 15
    Should be Equal as Numbers   ${res}    25

Substract Two Ints
    Set Tags    Substract   Ints
    ${res} =    Evaluate   10 - 15
    Should be Equal as Numbers   ${res}    -5

