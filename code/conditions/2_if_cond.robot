*** Test Cases ***

Exit Example
    ${text} =    Set Variable    ${EMPTY}
    FOR    ${var}    IN    one    two    three
        Run Keyword If    '${var}' == 'two'    Exit For Loop
        ${text} =    Set Variable    ${var}
    END
    Should Be Equal    ${text}    one


Continue Example
    ${text} =    Set Variable    ${EMPTY}
    FOR    ${var}    IN    one    two   three
        Continue For Loop If    '${var}' == 'two'
        ${text} =    Set Variable    ${var}
        Log To Console   ${text}
    END
    Should Be Equal    ${text}    three

