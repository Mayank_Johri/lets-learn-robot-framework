*** Variables ***
${txt} =  ""

*** Keywords ***
Execute ifelse example
    ${variable}   Set Variable    12
    ${txt} =  Set Variable If
    ...  ${variable} > 15  one
    ...  ${variable} < 5  two
    ...  ${variable} > 10  three
    [Return]   ${txt}
*** Test Cases ***
Validate if else
    ${val} =   Execute ifelse example
    log to Console   ${val}
