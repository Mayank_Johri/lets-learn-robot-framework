*** Variables ***
${txt} =   ""
${temp} =   13

*** Keywords ***
Execute ifelse example
    ${txt} =   Set Variable If
    ...   ${temp} > 30   Very Hot
    ...   ${temp} > 15   room temp
    ...   ${temp} > 5   Cold
    ...   ${temp} <= 5   Very Cold


    [Return]   ${txt}

*** Test Cases ***
Validate if else
    ${val} =   Execute ifelse example
    log to Console   \n${val}
