*** Variables ***    
${text}   ""

*** Keywords ***
Execute ifelse example
    ${variable}   Set Variable    12
    ${text} =   Set Variable If   ${variable} > 10 and ${variable} < 15   200
    [Return]    ${text}


*** Test Cases ***
Validate if else
    ${val} =   Execute ifelse example
    log to Console   ${val}
