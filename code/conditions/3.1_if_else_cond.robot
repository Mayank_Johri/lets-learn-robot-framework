*** Variables ***    
${text} =  ""
${temp1} =    12
${temp2} =    -12
${temp3} =    16

 
*** Keywords ***
 Convert Temp ${temp} To Sentance
    ${text}=    Run Keyword If   10 >= ${temp} < 0     Set Variable   Very Cold
    ...    ELSE    Run Keyword IF   15 >= ${temp} > 10    Set Variable   Cold
    ...    ELSE    Set Variable   Heat
    [Return]   ${text}

*** Test Cases ***
if else ${temp1}
    ${val} =   Run Keyword    Convert Temp ${temp1} To Sentance
    log To Console   ${val} 
     
if else ${temp2}
    ${val} =   Run Keyword    Convert Temp ${temp2} To Sentance
    log To Console   ${val} 

if else ${temp3}
    ${val} =   Run Keyword    Convert Temp ${temp3} To Sentance
    log To Console   ${val} 
