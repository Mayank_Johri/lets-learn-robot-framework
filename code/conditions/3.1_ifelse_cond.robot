*** Variables ***    
String  ${text} =  ""

*** Keywords ***
Execute ifelse example
    ${variable}   Set Variable    6
    ${text} =  Set Variable If
    ...  ${variable} > 5  one
    ...  ${variable} < 5  two
    ...  ${variable} = 5  three
    [Return]   ${text}


*** Test Cases ***
Validate if else
    ${val} =   Execute ifelse example  
    log to Console   ${val}  
