# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           lib/MathLib.py


*** Variables ***
${val1}  10
${val2}  200
${retval} "210"

*** Keywords ***
Add Int
    [Arguments]    ${val1} ${val2}
    ${ret} =       add int ${val1} ${val2}
    [return]       ${ret} 
    
*** Test Cases ***
Valid ints can be added
    $ret = Add Int ${val1} ${val2}
    Should Be Equal As Strings $ret $ret_val

