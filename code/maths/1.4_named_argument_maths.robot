# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py


*** Keywords ***
Get the Power
    [Arguments]    ${num}       ${power}
    ${ret} =       pow int      ${num}    ${power}
    [return]       ${ret}

*** Test Cases ***
Valid ints can be added
    ${ret} =    Get the Power      power=3  num=5
    Should Be Equal As Strings      ${ret}  125

