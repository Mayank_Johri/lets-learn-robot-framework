

def add(a, b, c=10):
    return a + b + c


if __name__ == '__main__':
    print(add(10, 20))
    print(add(b=10, a=20))
    print(add(10, b=20))
    print(add(c=222, a=20, b=12))
