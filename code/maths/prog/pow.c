#include<stdio.h>
#include<stdlib.h>
#include <math.h>


int main(int argc, char * argv[]) {
    int i, result  = 0;

    if (argc != 3) {
        printf("Please enter two int numbers.");
        exit(1);
    }

    result = pow(atoi(argv[1]), atoi(argv[2]));
    printf("%d", result);
    return(0);
}
