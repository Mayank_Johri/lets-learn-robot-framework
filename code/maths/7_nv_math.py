# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py

*** Variables ***
${val_1}    10
${val_2}    20.10
${ret_val}  30.10


*** Keywords ***
Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       add int   ${val1} 	${val2}
    [return]       ${ret}

*** Test Cases ***
Valid ints can be added
    ${ret} = 	Please Add Int      ${val_1}   	${val_2}
    Should Be Equal As Strings      ${ret} 	${ret_val}

