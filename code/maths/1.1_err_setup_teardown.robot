# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py

Test Setup        ${val_setup} =    1000


*** Keywords ***
Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       add int   ${val1} ${val2}
    [return]       ${ret} 
    
*** Test Cases ***
Valid ints can be added
    ${ret} = 	Please Add Int      ${val_setup}  100
    Should Be Equal As Strings   ${ret} 110

