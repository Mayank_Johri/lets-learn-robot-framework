# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py
Test Setup        Create File   ${file_name}
Test TearDown     Remove File   ${file_name}

*** Variables ***
${file_name}=  sum_total.txt

*** Keywords ***
Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       add int   ${val1}   ${val2}
    [return]       ${ret} 
    
*** Test Cases ***
Valid ints can be added
#    [Setup]   ${SETUP}
    ${ret} = 	Please Add Int    10    100
    Append to File   ${file_name}   10 100 ${ret}
    Should Be Equal As Strings   ${ret}  110


Valid ints can be added without teardown
    
    ${ret} = 	Please Add Int    101   102
    Append to File   ${file_name}   101 102 ${ret}
    Should Be Equal As Strings   ${ret}  203
    [TearDown]   NONE


Valid ints can be added without setup
    [Setup]   
    ${ret} = 	Please Add Int    101   102
    Append to File   ${file_name}   101 102 ${ret}
    Should Be Equal As Strings   ${ret}  203
