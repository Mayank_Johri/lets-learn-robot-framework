# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py


*** Keywords ***
Get the Power
    [Arguments]    @{junk}     ${num}=1     ${power}=2
    ${ret} =       pow int      ${num}    ${power}
    Log To Console     ${junk}
    [return]       ${ret}

*** Test Cases ***

Valid ints can be added
    ${ret} =    Get the Power      power=3   num=5
    Should Be Equal As Strings      ${ret}  125



Valid ints with partial named variable
    ${ret} =    Get the Power      3      5    num=10
    Should Be Equal As Strings      ${ret}  100


Valid ints without named variable
    ${ret} =    Get the Power      3      5
    Should Be Equal As Strings      ${ret}  125

