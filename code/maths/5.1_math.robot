# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/NewMathLib.py


*** Keywords ***
Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       enter two ints   ${val1} 	${val2}
    [return]       ${ret} 
    
*** Test Cases ***
Valid ints can be added
    ${ret} = 	Please Add Int      10   	100
    Should Be Equal As Strings      ${ret} 	110

