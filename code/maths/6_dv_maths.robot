# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/PAMathLib.py


*** Keywords ***
Please Add Int
    [Arguments]    ${val1}=10   ${val2}=20
    ${ret} =       add int   ${val1} 	${val2}
    [return]       ${ret} 

*** Test Cases ***
Valid ints can be added
    [Tags]    critical
    ${ret} = 	Please Add Int      10
    Should Be Equal As Strings      ${ret} 	30


Valid int & float can be added
    [Tags]    minor
    ${ret} = 	Please Add Int      10     20.3
    Should Be Equal As Strings      ${ret} 	30

