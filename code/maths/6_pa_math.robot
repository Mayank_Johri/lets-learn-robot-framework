# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/PAMathLib.py


*** Keywords ***
Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       add int   ${val1} 	${val2}
    [return]       ${ret} 
    
*** Test Cases ***
Valid ints can be added
    ${ret} = 	Please Add Int      10   	${EMPTY}
    Should Be Equal As Strings      ${ret} 	110

