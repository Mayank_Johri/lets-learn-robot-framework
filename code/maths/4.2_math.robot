# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py

*** Variables ***
${val_1}    10
${val_2}    20
${ret_val_int}   30
${ret_val}  enter two int numbers


*** Keywords ***
Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       add int   ${val1} 	${val2}
    [return]       ${ret} 

Execute Help
    ${ret} =   help
    [return]   ${ret}  
    
*** Test Cases ***
Valid ints can be added
    ${ret} = 	Please Add Int      ${val_1}    ${val_2}
    Should Be Equal As Strings      ${ret} 	${ret_val_int}


Validate Help Text
    ${ret} =   Execute Help
    Should Contain    ${ret}  ${ret_val}

