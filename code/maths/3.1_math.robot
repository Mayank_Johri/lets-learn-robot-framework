# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py

*** Variables ***
${ret_val}  "enter two int numbers"


*** Keywords ***
Please Add Int
    [Arguments]    ${val1}   ${val2}
    ${ret} =       add int   ${val1} 	${val2}
    [return]       ${ret} 

    
*** Test Cases ***
Valid ints can be added
    ${ret} = 	Please Add Int      ${EMPTY}    ${EMPTY}
    Should Be Equal As Strings      ${ret} 	${ret_val}

