# coding=utf8

import os
# import sys
import subprocess


class PAMathLib(object):

    def __init__(self):
        self._math_path = os.path.join(os.path.dirname(__file__),
                                       '..', 'prog', 'maths')

    def add_int(self, a, b=102):
        self._run_command(str(a), str(b))
        return self._status

    def help(self):
        self._run_command()
        return self._status

    def _run_command(self, *args):
        command = [self._math_path]
        command.extend(list(args))
        print(command)
        process = subprocess.Popen(command, universal_newlines=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        self._status = process.communicate()[0].strip()


if __name__ == '__main__':
    ml = MathLib()
    c = ml.add_int(110, 20)
    print(c)
    print(ml.help())
