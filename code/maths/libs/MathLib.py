# coding=utf8

import os
import subprocess


class MathLib(object):

    def __init__(self):
        self._math_path = os.path.join(os.path.dirname(__file__),
                                       '..', 'prog', 'maths')
        self._pow_path = os.path.join(os.path.dirname(__file__),
                                       '..', 'prog', 'pow')

    def add_int(self, a, b):
        self._run_command(0, str(a), str(b))
        return self._status

    def pow_int(self, a, b):
        self._run_command(1, str(a), str(b))
        return self._status

    def help(self):
        self._run_command()
        return self._status

    def _run_command(self, *args):
        args = list(args)
        _path = self._pow_path

        if len(args) > 0 and args[0] == 0:
            _path = self._math_path

        command = [_path]
        command.extend(list(args[1:]))
        print(command)
        process = subprocess.Popen(command, universal_newlines=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        self._status = process.communicate()[0].strip()


if __name__ == '__main__':
    ml = MathLib()
    c = ml.add_int(110, 20)
    print(c)
    print(ml.help())
    print(ml.pow_int(10, 20))
