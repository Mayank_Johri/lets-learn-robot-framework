# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py


*** Keywords ***
Get the Power
    [Arguments]    ${num}=10       ${power}=10
    ${ret} =       pow int      ${num}    ${power}
    [return]       ${ret}

*** Test Cases ***
Valid ints can be added
    ${ret} =    Get the Power      num=3    10
    Should Be Equal As Strings      ${ret}  1000

