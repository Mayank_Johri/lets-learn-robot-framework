# -*- coding: robot -*-

*** Settings ***
Library           OperatingSystem       # This should be ignored
Library           libs/MathLib.py

*** Variables ***
${num} =   10
${pow} =   4

*** Keywords ***
Get the Power
    [Arguments]    ${num}=10       ${power}=10
    ${ret} =       pow int      ${num}    ${power}
    [return]       ${ret}

*** Test Cases ***
Valid ints can be added
    [Documentation]   This test case *validates*     the _power_ function    Using 
	...                   ${num} and ${pow}
    ${ret} =    Get the Power      num=${num}    power=${pow}
    Should Be Equal As Strings      ${ret}  10000

